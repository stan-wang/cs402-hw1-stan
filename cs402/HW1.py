import matplotlib.pyplot as plt
import random
import time


class hw1:

    def __init__(self):
        pass

    def exercise1_1(fileName):
        lines = open(fileName).readlines()
        spice_dict = {}
        for line in lines:
            address = line.split()[1]
            if (address in spice_dict):
                spice_dict[address] = spice_dict[address] + 1
            else:
                spice_dict[address] = 1
        spice_dict = dict(sorted(spice_dict.items(), key=lambda item: item[0]))
        spice_x = list(spice_dict.keys())
        spice_x = list(map(str, spice_x))
        spice_y = list(spice_dict.values())
        plt.subplot(212)
        plt.bar(spice_x, spice_y)
        plt.show()

    def exercise1_2(fileName):
        lines = open(fileName).readlines()
        spice_dict = {}
        for line in lines:
            address = line.split()[0]
            if (address in spice_dict):
                spice_dict[address] = spice_dict[address] + 1
            else:
                spice_dict[address] = 1
        sum_v = 0
        for i in spice_dict.values():
            sum_v = sum_v + i
        frequency_read = spice_dict['0'] / sum_v
        frequency_write = spice_dict['1'] / sum_v
        print(frequency_read)
        print(frequency_write)

    def exercise2_1(matrix1, matrix2):
        new_matrix = [[0 for i in range(len(matrix1))] for j in range(len(matrix1))]
        for i in range(len(matrix1)):
            for j in range(len(matrix1)):
                for x in range(len(matrix1)):
                    new_matrix[i][j] += matrix1[i][x] * matrix2[x][j]

    def exercise2_2(matrix1, matrix2):
        new_matrix = [[0 for i in range(len(matrix1))] for j in range(len(matrix1))]
        for i in range(len(matrix1)):
            for j in range(len(matrix1)):
                for x in range(len(matrix1)):
                    new_matrix[i][j] += matrix1[x][j] * matrix2[i][x]


hw = hw1
hw.exercise1_1('spice.din')
hw.exercise1_1('tex.din')
hw.exercise1_2('spice.din')
hw.exercise1_2('tex.din')

matrix1 = [[random.randint(1, 10) for j in range(1, 3000)] for i in range(1, 300)]
matrix2 = [[random.randint(1, 100) for j in range(1, 3000)] for i in range(1, 300)]
matrix3 = [[round(random.uniform(1, 10), 2) for j in range(1, 3000)] for i in range(1, 300)]
matrix4 = [[round(random.uniform(1, 100), 2) for j in range(1, 3000)] for i in range(1, 300)]
start = time.time()
for i in range(5):
    hw.exercise2_1(matrix1, matrix2)
end = time.time()
print((end - start) / 5)

start = time.time()
for i in range(5):
    hw.exercise2_1(matrix3, matrix4)
end = time.time()
print((end - start) / 5)

start = time.time()
for i in range(5):
    hw.exercise2_2(matrix1, matrix2)
end = time.time()
print((end - start) / 5)

start = time.time()
for i in range(5):
    hw.exercise2_2(matrix3, matrix4)
end = time.time()
print((end - start) / 5)
